package ito.poo.clases;

public class Musicos {
	private String Titulo;
	private String Duracion;

	public Musicos(String titulo, String duracion) {
		super();
		Titulo = titulo;
		Duracion = duracion;
	}

	public String Alta(String titulo, String duracion) {
		this.Titulo = titulo; 
		this.Duracion = duracion;
		return titulo + " " + duracion;
	}

	public void Bajas() {
	}

	public void Consultas() {
	}

	public String getTitulo() {
		return Titulo;
	}

	public void setTitulo(String titulo) {
		Titulo = titulo;
	}

	public String getDuracion() {
		return Duracion;
	}

	public void setDuracion(String duracion) {
		Duracion = duracion;
	}
	
}
