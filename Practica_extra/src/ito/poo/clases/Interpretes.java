package ito.poo.clases;

public class Interpretes extends Musicos{
	public String generomusical;
	public String fechaderegistro;
	public String fechadeestreno;

	
	public Interpretes(String generomusical, String fechaderegistro, String fechadeestreno,String Titulo, String Duracion){
		super(Titulo, Duracion);
		
		this.generomusical = generomusical;
		this.fechaderegistro = fechaderegistro;
		this.fechadeestreno = fechadeestreno;
	}
	

	public String getGeneromusical() {
		return generomusical;
	}

	public void setGeneromusical(String generomusical) {
		this.generomusical = generomusical;
	}

	public String getFechaderegistro() {
		return fechaderegistro;
	}

	public void setFechaderegistro(String fechaderegistro) {
		this.fechaderegistro = fechaderegistro;
	}

	public String getFechadeestreno() {
		return fechadeestreno;
	}

	public void setFechadeestreno(String fechadeestreno) {
		this.fechadeestreno = fechadeestreno;
	}


	@Override
	public String toString() {
		return "Interpretes [generomusical=" + generomusical + ", fechaderegistro=" + fechaderegistro
				+ ", fechadeestreno=" + fechadeestreno + ", getTitulo()=" + getTitulo() + ", getDuracion()=" + getDuracion();
	}

}
