package ito.poo.clases;
import java.util.Formatter;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public class Archivotex {
	public static void grabaDatos(ArrayList<Interpretes> l) throws FileNotFoundException {
		Formatter archivo;
		
		archivo = new Formatter(new File("datos.txt"));
		for(Interpretes inter :l) {
			archivo.format("%s,%s\n",inter.getTitulo(), inter.getDuracion());
		}
		archivo.close();
	}
	
	public static ArrayList<Interpretes> leerDatos() throws FileNotFoundException{
		ArrayList<Interpretes> l =new ArrayList<Interpretes>();
		Scanner file=new Scanner(new File("datos.txt"));
		while(file.hasNext()) {
			Scanner linea = new Scanner(file.nextLine());
			linea.useDelimiter(";");
			
			String Titulo = linea.next();
			String Duracion = linea.next();
			l.add(new Interpretes("","","","",""));
			
		}
		return l;
	}

}